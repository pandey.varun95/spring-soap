package com.example;

public class Student {
	private String id;

	public Student() {
		super();
	}

	public Student(String id) {
		super();
		this.id = id;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return String.format("Student [id=%s]", id);
	}

}